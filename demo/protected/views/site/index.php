<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;

?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js" type="text/javascript"></script>

	<form method="post">
		        <div class='col-sm-6'>
		            <input type='text' name="start_date" placeholder="Start Date" class="form-control" id='startDate' />
		        </div>
		        <div class='col-sm-6'>
		            <input type='text' name="end_date" class="form-control" placeholder="End Date" id='endDate' />
		        </div>
		        <script type="text/javascript">
		            $(function () {
		                $('#startDate').datepicker({
		                    format:'dd-mm-yyyy',
		                    autoclose: true
		                });
		                $('#endDate').datepicker({
		                   format:'dd-mm-yyyy' ,
		                   autoclose: true
		                });

		            });
		        </script>
		<input type="submit" name="submit" value="Submit">
	</form>
