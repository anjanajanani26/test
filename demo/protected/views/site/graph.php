<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;

?>
<script src="https://www.chartjs.org/dist/2.8.0/Chart.min.js"></script>
<script src="https://www.chartjs.org/samples/latest/utils.js"></script>
<style>
	canvas{
		-moz-user-select: none;
		-webkit-user-select: none;
		-ms-user-select: none;
	}
</style>

<div class="content">
	<br>
	Fastest Asteroid in km/h : ID - <?php echo $fastest['id']." / ".$fastest['kmh']; ?>
	<br>
	Closest Asteroid : ID - <?php echo $closest['id']." / ".$closest['dist']; ?>
	<br>
	<br>
	<div class="wrapper"><canvas id="chart-0"></canvas></div>
</div>

<?php
	foreach ($dates as $value) {
		
		//echo '<script>x.push();</script>';

	}
?>
<script type="text/javascript">
	var DATA_COUNT = 12;

		var utils = Samples.utils;

		utils.srand(110);

		function getLineColor(ctx) {
			return utils.color(ctx.datasetIndex);
		}

		function alternatePointStyles(ctx) {
			var index = ctx.dataIndex;
			return index % 2 === 0 ? 'circle' : 'rect';
		}

		function makeHalfAsOpaque(ctx) {
			return utils.transparentize(getLineColor(ctx));
		}

		function adjustRadiusBasedOnData(ctx) {
			var v = ctx.dataset.data[ctx.dataIndex];
			return v < 10 ? 5
				: v < 25 ? 7
				: v < 50 ? 9
				: v < 75 ? 11
				: 15;
		}

		function generateData() {
			return utils.numbers({
				count: DATA_COUNT,
				min: 0,
				max: 100
			});
		}

		var data = {
			//labels: utils.months({count: DATA_COUNT}),
			labels: <?php echo json_encode($dates);?>,
			datasets: [{
				data: <?php echo json_encode($yLabel);?>
			}]
		};

		var options = {
			legend: false,
			tooltips: true,
			elements: {
				line: {
					fill: false,
					backgroundColor: getLineColor,
					borderColor: getLineColor,
				},
				point: {
					backgroundColor: getLineColor,
					hoverBackgroundColor: makeHalfAsOpaque,
					radius: adjustRadiusBasedOnData,
					pointStyle: alternatePointStyles,
					hoverRadius: 15,
				}
			}
		};

		var chart = new Chart('chart-0', {
			type: 'line',
			data: data,
			options: options
		});
</script>