<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$curl = curl_init();
		$url = "https://api.nasa.gov/neo/rest/v1/feed?api_key=gB0Ds45SbQDwuQd7sJfpcVNZ3NZH6O6rwEqvVD51";
		$error = '';
		
		if (isset($_POST['submit'])) {

			$data = $_POST;
			if ($data) {

				$url = $url."&start_date=".date('Y-m-d',strtotime($_POST['start_date']))."&end_date=".date('Y-m-d',strtotime($_POST['end_date']));
				curl_setopt($curl, CURLOPT_URL, $url);
			    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			    $result = curl_exec($curl);

			    curl_close($curl);

			    if (!empty($result)) {
			    	$data = json_decode($result,true);

			    	if (isset($data['near_earth_objects']) && !empty($data['near_earth_objects'])) {

			    		foreach ($data['near_earth_objects'] as $key => $value) {
			    			
			    			$dataSets[strtotime($key)] = count($value);
			    			$dates[] = strtotime($key);
			    			$tempResult[] = [
			    				$key => count($value)
			    			];

			    			if (!empty($value)) {

			    				foreach ($value as $label => $v) {
			    					
			    					if (isset($v['close_approach_data']) && isset($v['close_approach_data'][0])) {


			    						if (isset($v['close_approach_data'][0]['relative_velocity'])) {

				    						$kmh = $v['close_approach_data'][0]['relative_velocity']['kilometers_per_hour'];

				    						if (!isset($fastest['kmh']) || $fastest['kmh'] < $kmh) {
				    							$fastest = [
				    								'kmh' => $kmh,
				    								'id' => $v['id']
				    							];
				    						}

			    						}

			    						if (isset($v['close_approach_data'][0]['miss_distance'])) {

				    						$dist = $v['close_approach_data'][0]['miss_distance']['kilometers'];

				    						if ( !isset($closest['dist']) || $closest['dist'] < $dist) {
				    							$closest = [
				    								'dist' => $dist,
				    								'id' => $v['id']
				    							];
				    						}

			    						}

			    					}

			    				}
			    			}
			    		}

			    		if (!empty($dataSets)) {

			    			asort($dataSets);
			    			asort($dates);

			    			if (!empty($dates)) {

			    				foreach ($dates as $value) {
			    					
			    					$xLabel[] = date('Y-m-d',$value);
			    					$yLabel[] = $dataSets[$value];
			    				}
			    			}
			    		}
			    	}
			    	$this->render('graph',array('yLabel' => $yLabel,'dates' => $xLabel,'fastest' => $fastest, 'closest' => $closest));
			    	exit();
			    } else {
			    	$error = "No data available";
			    }

	        }

		}
		$this->render('index',array('error' => $error));
	}

	/*public function actionGraph() {

		$error = 'No Data available';
		if (!empty($result)) {


		}

		$this->render('graph',array('error' => $error));
	}

*/	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}